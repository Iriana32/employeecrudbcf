<?php

namespace App\Form;

use App\Entity\Employee;
use App\Entity\Experiences;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom : '
            ])
            ->add('lastName', null, [
                'label' => 'Prénom : '
            ])
            ->add('age', null, [
                'label' => 'Age : '
            ])
            ->add('position', null, [
                'label' => 'Poste : '
            ])
            ->add('experience', EntityType::class, [
                'class' => Experiences::class,
                'choice_label' => 'titre',
                'label' => 'Experiences'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
