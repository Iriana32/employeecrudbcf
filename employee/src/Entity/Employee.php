<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=EmployeeRepository::class)
 * @ApiResource(
 * normalizationContext={"groups"={"read:employee"}},
 * collectionOperations={"get", "post"},
 * itemOperations={"get", "put", "delete"}
 * )
 */
class Employee
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read:employee"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:employee"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:employee"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"read:employee"})
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"read:employee"})
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity=Experiences::class, inversedBy="employees")
     * @Groups({"read:employee"})
     */
    private $experience;

    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getExperience(): ?Experiences
    {
        return $this->experience;
    }

    public function setExperience(?Experiences $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    
}
